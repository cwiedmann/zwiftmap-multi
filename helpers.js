/**
* Loads a CSS file from the supplied URL
* @param {String} url    The URL of the CSS file, if its relative
                         it will be to the current page's url
* @return {HTMLElement}  The <link> which was appended to the <head>
*/
exports.loadcss = (url) => {
  var head = document.getElementsByTagName('head')[0],
  link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = url;
  head.appendChild(link);
  return link;
}

/**
* Insert inline CSS file from the supplied string
* @param {String} css    The CSS file
* @return {HTMLElement}  The <style> which was appended to the <head>
*/
exports.inlinecss = (css) => {
  var head = document.getElementsByTagName('head')[0],
  style = document.createElement('style');
  style.innerHTML = css;
  head.appendChild(style);
  return style;
}
