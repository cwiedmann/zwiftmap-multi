// This file is required by the network.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const app = require('electron').remote.app
const fs = require('fs')
const helpers = require('./helpers.js')

usercss = app.getPath('documents') + "/Zwift/zwiftmap/network.css"

fs.access(usercss, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
    helpers.loadcss(usercss)
  }
})

const Counter = require('./counters.js')

let allCounters = []
allCounters[2] = new Counter(2,20,10)
allCounters[4] = new Counter(4,20,10)
allCounters[5] = new Counter(5,20,10)
allCounters[6] = new Counter(6,20,10)
allCounters[7] = new Counter(7,20,10)
allCounters['nc'] = new Counter('nc',20,1)
allCounters['critical'] = new Counter('critical',999999999,1)

console.dir(allCounters)

for (var c in allCounters) {
  var counter = allCounters[c]
  console.dir(counter)
	$('#counters').append('<div id="error-' + counter.id + '" class="error-counter">0</div>\n')
	
	function refresh(counter) {
		// console.log('firing refresh ' + counter.id);
		$('#error-' + counter.id).text(counter.count)
	}
	
  // use setinterval to refresh counters and update display every X seconds
	var timer = setInterval(refresh, 100, counter);
}


const {ipcRenderer} = require('electron')

var patterns = {
	hhmmss : /(\d+):(\d+):(\d+)/ ,
  errorno :    /[^\(]*\((\d+)\)/ ,
	networkerror :  /network:error/i ,
	netclienterror :  /netclient:error/i ,
  criticalerror : /:\suh\soh/i
}

let day = 0

// ipc messages to handle:
// network
ipcRenderer.on('network', (event, timestamp, type, message) => {
  hhmmssMatch = patterns.hhmmss.exec(timestamp)
  // calculate seconds as seconds from midnight of the first day of the first timestamp received
  seconds = day*24*60*60 + hhmmssMatch[1]*60*60 + hhmmssMatch[2]*60 + hhmmssMatch[3]
  if (seconds < day*24*60*60) {
    day += 1  // log spans across midnight
    seconds = day*24*60*60 + hhmmssMatch[1]*60*60 + hhmmssMatch[2]*60 + hhmmssMatch[3]
  }

	//console.log(type)
	
  var errorid = null

  if (patterns.networkerror.test(type)) {
    //console.log('network:error')
    errornoMatch = patterns.errorno.exec(message)
	  errorid = errornoMatch[1]
  } else if (patterns.netclienterror.test(type)) {
    errorid = ( patterns.criticalerror.test(message) ? 'critical' : 'nc' ) 
  }

	// console.log(errorid)
	if (errorid) {
    if (allCounters[errorid]) {
      allCounters[errorid].add(seconds)
      $('#error-' + errorid).text(allCounters[errorid].count)
    }
  }

  $('#network').prepend('<div class="networkmessage"><div class="message">' + timestamp + ' ' + seconds + ' ' + type + ' ' + message + '</div></div>')
})

ipcRenderer.on('ignore-mouse', (event, ignoreMouseEvents) => {
  if (ignoreMouseEvents) {
      // hide visual indicators
      $('html').removeClass('allow_interaction')
    } else {
      // show visual indicators that this window accepts mouse events
      $('html').addClass('allow_interaction')
  }
})
