﻿#SingleInstance, force
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.




; -- now process the log itself

; fallback value is log.txt (parsed unless another log file was dropped onto the script)
FileName := A_ScriptDir "\log.test.txt"
msgbox, % FileName

Loop %0%  ; For each parameter (or file dropped onto a script):
{
    GivenPath := %A_Index%  ; Fetch the contents of the variable whose name is contained in A_Index.
    Loop %GivenPath%, 1
        FileName = %A_LoopFileLongPath%
    ;MsgBox The case-corrected long path name of file`n%GivenPath%`nis:`n%LongPath%
}


OutFile := A_ScriptDir "\log.txt"
FileDelete, %OutFile%


OutTxt := ""

;Loop, READ, %FileName%, %OutFile%
Loop, READ, %FileName%
{

  TestString := Trim(A_LoopReadLine )
  OutLine := TestString

  ;FileAppend, %OutLine%`r`n, % OutFile
  ;FileAppend, %OutLine%, % OutFile
file := FileOpen(OutFile, "a")
    file.WriteLine(OutLine)
file.Close()

;  Sleep, 1

}

