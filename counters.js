

class Counter {
	constructor(id, period, threshold) {
		this.id = id;
		this.period = + period; // in seconds
		this.threshold = + threshold; // when is number of errors per period critically high?
		this.occurrences = []
		this.timer = undefined
	}

	timeout(when) {
		var _when = + when;

		// clear any old timer
		if (this.timer) {
			clearTimeout(this.timer)
		}
		// setTimeout (using refresh(timestamp+period) and period as interval length)
		// console.log('will set timeout in counter ' + this.id + ' : ' + when);
		var _this = this;
		this.timer = setTimeout(this.refresh.bind(this), 200, _when);
		// console.dir(this.timer);
	}
		
	refresh(timestamp) {
		var _timestamp = + timestamp
		while ((_timestamp - this.occurrences[0]) > this.period) {
			this.occurrences.shift()
		}
		this.timeout(_timestamp + 2);
	}
	
	add(timestamp) {
		var _timestamp = + timestamp;
		// console.log(this.id + ' ' + timestamp);
		this.occurrences.push(_timestamp);
		this.refresh(_timestamp);
	}
		
	get count() {
		return this.occurrences.length;
	}	



		
}

module.exports = Counter;