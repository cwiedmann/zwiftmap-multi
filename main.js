const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
// Module for global shortcuts
const globalShortcut = electron.globalShortcut
//
const windowStateKeeper = require('electron-window-state');
//
const Menu = electron.Menu
//
const hasFlag = require('has-flag');
//
const {ipcMain} = require('electron')
//
const Config = require('electron-config');
const config = new Config();


const is = require('electron-is');
const log = require('electron-log');

setLogLevel( (is.dev) ? 'info' : 'error' )


// globals 

global.color = { 'use-default-color': true }
global.background = '0.0'
global.track = { 'me': true, 'followees': true }
global.log = { 'file': log.transports.file.findLogPath() }


// Initialise state variables
let state = {ignoreMouseEvents: true, asOverlay: {map: true, chat: true} }

// Default values for options
let miscOptions = { asOverlay: true }

let windowOptions = {
  // all: { asOverlay: true },
  main: { 
    defaults: { width: 380, height: 750 },
    windowName: 'main',
    fileName: 'config.html',
    browserOptionsWindow: { },
    allowOverlay: false
  },
  chat: {
    defaults: {width: 450, height: 300}, 
    windowName: 'chat',
    fileName: 'chat.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false}, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: true, backgroundColor: '#802c2c2c'},
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  },
  network: {
    defaults: {width: 450, height: 140}, 
    windowName: 'network',
    fileName: 'network.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false}, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: true, backgroundColor: '#802c2c2c'},
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  },
  stat: {
    defaults: {width: 1000, height: 150}, 
    windowName: 'stat',
    fileName: 'stat.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false}, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: false}, // , backgroundColor: '#802c2c2c'},
    allowOverlay: true,
    onlyOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: false
  },
  map: {
    defaults: {width: 600, height: 600},
    windowName: 'map',
    fileName: 'map.html',
    browserOptionsOverlay: {alwaysOnTop: true, transparent: true, frame: false}, // , backgroundColor: undefined},
    browserOptionsWindow: {alwaysOnTop: true, transparent: false, frame: true, backgroundColor: '#802c2c2c'},
    allowOverlay: true,
    allowToggleWindowState: true,
    allowIgnoreMouse: true
  },
  login: {
    defaults: { width: 400, height: 350 },
    windowName: 'login',
    fileName: 'login.html',
    browserOptionsWindow: { alwaysOnTop: true, frame: false }, 
    allowOverlay: false
  },
  ghosts: {
    defaults: { width: 300, height: 500 },
    windowName: 'ghosts',
    url: '/editghosts',
    browserOptionsWindow: { }, 
    allowOverlay: false
  }
}


// Configuration / options
if (process.env.ZWIFTMAP_WINDOW == 1 || hasFlag('window') || (config.has('mode.window') ? config.get('mode.window') : false)) {
	global.mode = { 'window': true }
  miscOptions.asOverlay = false
  // state.asOverlay = {map: false, chat: false, stat: false}
  state.asOverlay = {map: false, chat: false}
  state.ignoreMouseEvents = false

  // app.disableHardwareAcceleration can only be called before app is ready
  // Therefore only called if normal window mode is forced
  // Necessary if OBS Studio has to be able to capture the window:
  app.disableHardwareAcceleration()

}

if ((config.has('graphics.disable-hw-acceleration') ? config.get('graphics.disable-hw-acceleration') : false) && hasFlag('disablehwacc')) {
  // disable if option is explicitly set  
  app.disableHardwareAcceleration()
}


// console.log('Options:\n' + JSON.stringify(m {
  // disable if option is explicitly set  iscOptions,null,4) + '\n' + JSON.stringify(browserOptionsMap,null,4))
  

// menu template
let template = [
  { label: app.getName(), submenu: [
    { label: 'Toggle window state', click() { actionOnGlobalShortcut() } },
    { type: 'separator' },
    { role: 'quit' }
  ] }
]

const menu = Menu.buildFromTemplate(template)

const path = require('path')
const url = require('url')

// Keep global references of the window objects, if we don't, the windows will
// be closed automatically when the JavaScript object is garbage collected.
let windows = { chat: null, map: null, network: null, main: null, login: null, ghosts: null, stat: null }

// the window names are used later when handling the window states
const windowNameList = ['main','map','chat','network', 'login', 'ghosts','stat']

// collection of windowStateKeepers
let stateKeepers = []


/**
 * 
 */
function createMainWindow () {
  createWindow('main', false)
}

/**
 * 
 */
function createMapWindow () {
  createWindow('map', state.asOverlay.map)
}

/**
 * 
 */
function createChatWindow () {
  createWindow('chat', state.asOverlay.chat)
}

/**
 * 
 */
function createNetworkWindow () {
  createWindow('network', state.asOverlay.network)
}

/**
 * 
 */
function createStatWindow () {
  createWindow('stat', state.asOverlay.stat)
  // createWindow('stat', true)
}

/**
 * 
 * @param {*} name 
 * @param {*} asOverlay 
 */
function createWindow (name, asOverlay) {
  // Load the previous state with fallback to defaults
  let windowState = windowStateKeeper({
    defaultWidth: windowOptions[name].defaults.width,
    defaultHeight: windowOptions[name].defaults.height,
    file: 'window-state-' + windowOptions[name].windowName + '.json'
  });
  stateKeepers.push(windowState)
  
  let browserOptions

  if (asOverlay) {
    browserOptions = windowOptions[name].browserOptionsOverlay
  } else {
    browserOptions = windowOptions[name].browserOptionsWindow
  }

  console.log(JSON.stringify(windowOptions[name], null, 4))

  browserOptions.width = windowState.width
  browserOptions.height = windowState.height
    
  if (windowState.x == undefined) {
    defaultPosition = getDefaultPosition(name, browserOptions.width, browserOptions.height)
    browserOptions.x = defaultPosition.x
    browserOptions.y = defaultPosition.y
  } else {
    browserOptions.x = windowState.x,
    browserOptions.y = windowState.y
  }


  if (name == 'main' && windowState.x !== undefined) {
    // make sure that the main/config window is shown visibly on an active display
    // if the saved position is outside the currently visible area
    let mainDisplay = electron.screen.getDisplayNearestPoint({x: browserOptions.x, y: browserOptions.y})
    console.log(mainDisplay)
    if ((mainDisplay.bounds.x <= browserOptions.x) && (mainDisplay.bounds.x + mainDisplay.bounds.width > browserOptions.x) && (mainDisplay.bounds.y <= browserOptions.y) && (mainDisplay.bounds.y + mainDisplay.bounds.height > browserOptions.y)) {
      // all is ok
    } else {
      // calculate a new position
      browserOptions.x = mainDisplay.bounds.x + (mainDisplay.bounds.width - browserOptions.width) / 2
      browserOptions.y = mainDisplay.bounds.y + (mainDisplay.bounds.height - browserOptions.height) / 2
    }
  }

	// Create the browser window.
  browserOptions.show = true // false  // Why does false cause a white background in overlay mode???
	windows[name] = new BrowserWindow(browserOptions)
  
  windows[name].once('ready-to-show', () => {
    windows[name].show()
  })

  // Let us register listeners on the window, so we can update the state
  // automatically (the listeners will be removed when the window is closed)
  // and restore the maximized or full screen state
  windowState.manage(windows[name]);

  // and load the index.html of the app.
  const windowUrl = windowOptions[name].fileName
      ? url.format({
          pathname: path.join(__dirname, windowOptions[name].fileName),
          protocol: 'file:',
          slashes: true
        })
      : `http://localhost:${serverPort}${windowOptions[name].url}`;
  windows[name].loadURL(windowUrl)


  if (asOverlay) {
 		// Set application menu IF on mac AND asOverlay
 		if (process.platform == 'darwin') { Menu.setApplicationMenu(menu) }

	  // if (name != 'main' ) {
	  if (windowOptions[name].allowOverlay) {
      // The main window itself is not shown as an overlay, but...
      // for all windows except main: 
      windows[name].setIgnoreMouseEvents(state.ignoreMouseEvents && (windowOptions[name].allowIgnoreMouse))
    }
  }


  // Open the DevTools.
  if (hasFlag('devtools')) {
    windows[name].webContents.openDevTools()
  }

  // Emitted when the window is closed.
  windows[name].on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    windows[name] = null
    if (name == 'main' ) {
      // closing the main window will quit the application if on windows
      if (process.platform !== 'darwin') {
        app.quit()
      }
    } else if (name == 'ghosts' && windows.map) {
      windows.map.webContents.send('clear-activity');
    }
  })
}


//
function getDefaultPosition(name, width, height) {
  var position = {x: 0, y: 0}
  if (windows[name]) {
    if (name == 'chat') {
      position.x = windows['main'].getBounds().x + windows['main'].getBounds().width
      position.y = windows['main'].getBounds().y
    } else if (name == 'map') {
      position.x = windows['main'].getBounds().x - width
      position.y = windows['main'].getBounds().y
    } else if (name == 'network') {
      position.x = windows['main'].getBounds().x + windows['main'].getBounds().width
      position.y = windows['main'].getBounds().y + windowOptions['chat'].defaults.height
    } else if (name == 'stat') {
      position.x = windows['main'].getBounds().x 
      position.y = windows['main'].getBounds().y - windowOptions['stat'].defaults.height
    }
  }
  return position
}

// riderLogData and locationData objects
const RiderLogData = require('./rider-log-data')
const LocationData = require('./location-data')
const riderLogData = new RiderLogData()
const locationData = new LocationData(riderLogData)

let server = null,
    serverPort = null
/**
 * 
 */
function createLocalServer() {
  const Server = require('zwift-second-screen/server/server')
  server = new Server({
    subscribe: () => locationData.subscribe(),
    getRider: () => locationData
  })
  server.start(serverPort)
}

/**
 *  Event handler for when we have updated position data
 */
function mapWinOnPosition(positions) {
  if (windows.stat) {
    if (positions[0] && positions[0].me) {
      windows.stat.webContents.send('new-stat', JSON.stringify(positions[0]))
    }
  }
  if (windows.map) {
    // log.info(positions);
    windows.map.webContents.send('set-position', JSON.stringify(positions));
  }
}
//
locationData.on('positions', mapWinOnPosition)

// Chat data object (for playername resolution)
const ChatData = require('./chat-data')
const chatData = new ChatData()

// Event handler for when we have new chat messages
chatData.on('message', (msg) => {
  // console.log('chatWinOnMessage: ' + msg[0].timestamp + ' ' + JSON.stringify(msg))
  if (windows.chat) {
    windows.chat.webContents.send('chat', msg[0].timestamp, msg[0].playerID, msg[0].message, msg[0].firstName, msg[0].lastName);
  }
})


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createMainWindow()

  if (miscOptions.asOverlay) {
	  // Register a 'Super+Z' shortcut listener.
	  const ret = globalShortcut.register('Alt+Super+Z', actionOnGlobalShortcut)

	  if (!ret) {
		console.log('registration failed')
	  }
  }

})


/**
 * 
 */
function actionOnGlobalShortcut () {
	console.log('Alt+Super+Z is pressed')
	state.ignoreMouseEvents = !state.ignoreMouseEvents
	console.log('changed state to \n' +  JSON.stringify(state, null, 4))

  windowNameList.forEach( (name) => {
    if ((windowOptions[name]) && (windowOptions[name].allowToggleWindowState)) {
      if (windows[name]) {
        windows[name].setIgnoreMouseEvents(state.ignoreMouseEvents && (windowOptions[name].allowIgnoreMouse));
        windows[name].webContents.send('ignore-mouse', state.ignoreMouseEvents)
      }
    }
  })
  
}


// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

//
app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (windows['main'] === null) {
    createMainWindow()
  }
})

//
app.on('will-quit', () => {
  // Unregister a shortcut.
  // globalShortcut.unregister('Alt+Super+Z')
  // Unregister all shortcuts.
  globalShortcut.unregisterAll()

  tail.unwatch()
})


let locationDataUnsubscribe;
//
ipcMain.on('show-map', (event, showMap, asOverlay) => {
  // subscribe to position data when map or stat is open
  if (showMap !== !!windows.map) {
    if (locationDataUnsubscribe && !windows.stat) {
      locationDataUnsubscribe()
      locationDataUnsubscribe = null
    } 

    if (showMap && !locationDataUnsubscribe) {
      locationDataUnsubscribe = locationData.subscribe()
    }
  }

  showWindow('map', showMap, asOverlay)
})


//
ipcMain.on('show-chat', (event, showChat, asOverlay) => {
  showWindow('chat', showChat, asOverlay)
})

//
ipcMain.on('show-stat', (event, showStat, asOverlay) => {
  // subscribe to position data when map or stat is open
  if (showStat !== !!windows.stat) {
    if (locationDataUnsubscribe && !windows.map) {
      locationDataUnsubscribe()
      locationDataUnsubscribe = null
    } 

    if (showStat && !locationDataUnsubscribe) {
      locationDataUnsubscribe = locationData.subscribe()
    }
  }

  showWindow('stat', showStat, asOverlay)
})

//
ipcMain.on('show-network', (event, showNetwork, asOverlay) => {
  showWindow('network', showNetwork, asOverlay)
})

/**
 * 
 * @param {*} name 
 * @param {*} show 
 * @param {*} asOverlay 
 */
function showWindow(name, show, asOverlay) {
   // force overlay if windowOptions says so
   asOverlay = asOverlay || (windowOptions[name].onlyOverlay == true)

   // prevent overlay if windowOptions says so
   asOverlay = asOverlay && (windowOptions[name].allowOverlay == true)

   
   if (show) {
    // if window already exists with same overlay status then do nothing
    if (windows[name] && (asOverlay == state.asOverlay[name])) {
      // no change
      return
    } 
    if (windows[name]) {
      // close existing window
      windows[name].close()
    }
    if (asOverlay != state.asOverlay[name]) {
      // change options
      if (windowOptions[name].allowOverlay) {
        // this window is in the set of windows forming the overlay.
        // the global asOverlay and ignoreMouseEvenst status is updated 
        // to reflect the global overlay status.
        miscOptions.asOverlay = asOverlay
        state.ignoreMouseEvents = asOverlay
      }
      state.asOverlay[name] = asOverlay
    }
    createWindow(name, asOverlay)
  } else {
    // remove window if it exists
    if (windows[name]) {
        windows[name].close()
    }
  }
}

//
ipcMain.on('gather-windows', () => {
  console.log('gather-windows received')
  windowNameList.forEach((name, index, arr) => {
    if (windows[name] && name != 'main') {
      gatherWindow(name)
    }
  })
})

function gatherWindow(name) {
  console.log('gatherWindow ' + name)
  let mainWindow = windows.main
  let mainDisplay = electron.screen.getDisplayNearestPoint({x: mainWindow.getBounds().x, y: mainWindow.getBounds().y})

  if (windows[name]) {
    currentWindow = windows[name]
    currentDisplay = electron.screen.getDisplayNearestPoint({x: currentWindow.getBounds().x, y: currentWindow.getBounds().y})

    let offsetX = currentWindow.getBounds().x - currentDisplay.bounds.x;
    let offsetY = currentWindow.getBounds().y - currentDisplay.bounds.y;

    currentWindow.setPosition(mainDisplay.bounds.x + offsetX, mainDisplay.bounds.y + offsetY)
  }
}

//
ipcMain.on('default-positions', () => {
  console.log('default-positions received')
  windowNameList.forEach((name, index, arr) => {
    if (windows[name] && name != 'main') {
      defaultPosition = getDefaultPosition(name, windows[name].getBounds().width, windows[name].getBounds().height)
      windows[name].setPosition(defaultPosition.x, defaultPosition.y)
    }
  })
})



//
ipcMain.on('run-server', (event, runServer, port) => {
  if (runServer) {
    if (server) {
      // if server already running then do nothing
      if (port === serverPort) return
      // stop and start on a different port
      server.stop()
    }
    // else start the server
    serverPort = port
    createLocalServer()
  } else {
    // stop server if it's running
    if (server) {
      server.stop()
      server = null
    }
  }
})

//
ipcMain.on('set-world', (event, world) => {
	  if (windows['map']) {
      windows['map'].webContents.send('set-world', world);
      locationData.setWorld(world)
    }
})

//
ipcMain.on('change-color', (event, color) => {
  global.color = color
  console.log(global.color)
  if (windows['map']) {
    windows['map'].webContents.send('change-color')
  }
  event.returnValue = true
})

//
ipcMain.on('change-background', (event, opacity) => {
  global.background = '' + opacity + ''
  console.log(global.background)
  if (windows['map']) {
    windows['map'].webContents.send('change-background')
  }
  event.returnValue = true
})

//
ipcMain.on('change-trackers', (event, track) => {
  global.track = track
  if (windows['map']) {
    windows['map'].webContents.send('change-trackers')
  }
  event.returnValue = true
})

//
ipcMain.on('clear-window-state', (event) => {
  stateKeepers.forEach( (windowState, index, arr) => {
    windowState.unmanage()
    // windowState = null
    // arr.splice(index, 1)
  })
  windowNameList.forEach( (windowName) => {
      fs.unlink(app.getPath('userData')  + '/window-state-' + windowName + '.json', (err) => {
        if (err) return;
      })
  })
})

//
ipcMain.on('toggle-window-state', (event) => {
  console.log('received toggle-window-state', miscOptions.asOverlay );
  if (miscOptions.asOverlay) {
    actionOnGlobalShortcut()
  }
})


let signInSender
//
ipcMain.on('begin-sign-in', (event, username, password) => {
  signInSender = event.sender
  showWindow('login', true, false)
})

//
ipcMain.on('begin-silent-sign-in', (event, username, password) => {
  signInSender = event.sender 
})
//
ipcMain.on('cancel-sign-in', (event) => {
  showWindow('login', false, false)
})

//
ipcMain.on('sign-in', (event, username, password) => {
  const ZwiftAccount = require('zwift-mobile-api')
  const Rider = require('zwift-second-screen/server/rider')

  // Sign in user
  const account = new ZwiftAccount(username, password);
  const rider = new Rider(account);
  // use the account when chat messages are processed
  chatData.setAccount(account);

  // Get the profile (to check the credentials and get a name)
  rider.getProfile()
    .then(profile => {
      // Hide the sign-in window
      showWindow('login', false, false)

      // Set source to API data
      locationData.setSource(rider);

      const name = `${profile.firstName} ${profile.lastName}`
      signInSender.send('signed-in', name)
      console.log(`sign-in as ${name}`)

      log.info(profile)
      
      // 
      // rider.getStatic()

    })
    .catch(error => {
      // Show the error on the sign-in window
      event.sender.send('error', error)
    });
})

//
ipcMain.on('sign-out', (event, username, password) => {
  // Set source back to log data
  locationData.setSource(riderLogData);
  // Set account used for chatData to null
  chatData.setAccount(null);

  console.log('sign-out');
})

ipcMain.on('edit-ghosts', () => {
  showWindow('ghosts', true, false)
})

ipcMain.on('preview-activity', (event, riderId, activityId) => {
  if (windows.map) {
    const ghostsApi = locationData.getGhosts();
    if (ghostsApi && ghostsApi.getActivity && activityId) {
      ghostsApi.getActivity(riderId, activityId).then(activity => {
        windows.map.webContents.send('preview-activity', JSON.stringify(activity));
      });
    } else {
      windows.map.webContents.send('clear-activity');
    }
  }
})

//
ipcMain.on('set-log-level', (event, loglevel) => {
  loglevel = ((loglevel == 'false') ? false : loglevel);
  setLogLevel(loglevel);
})

//
function setLogLevel(loglevel) {
  if (is.dev) {
    log.transports.file.level = loglevel;
    log.transports.console.level = loglevel;
  } else {
    log.transports.file.level = loglevel;
    log.transports.console.level = false;
  }
  console.log('Log level is ' + log.transports.file.level);
}

// Tail on log.txt is established

var Tail = require('always-tail');
var fs = require('fs');

var filename = app.getPath('documents') + "/Zwift/Logs/Log.txt";
if (hasFlag('test')) filename = './project_test/log.txt'
console.log(filename)
if (!fs.existsSync(filename)) fs.writeFileSync(filename, "");

// TO DO: Guess world by reading log initially OR reading prefs.xml


var tail = new Tail(filename, '\n', { interval: 50 });
// interval is set explicitly to ensure sufficiently frequent polling

var patterns = {
	position : /\[([^\]]*)\]\s+FPS\s*\d{1,3},\s*(\S+),\s*(\S+),\s*(\S+)/ ,
  world :    /\[([^\]]*)\]\s+Loading WAD file 'assets\/Worlds\/world(\d*)\/data.wad/ ,
	chat :     /\[([^\]]*)\]\s+NETWORK:Heard area message from (\d*) \((.*)\)/ ,
	network :  /^\[([^\]]*)\]\s+(network:error|network:delayed|warn\s+:\s+invalid\sroad\stime|netclient:error)(.*)$/i
}

//
tail.on('line', function(data) {
  // console.log("got line:", data);

  // checking for pattern matches (most likely match is checked first (TODO: verify optimal order))
  if (match  = patterns.position.exec(data)) {
    // position
	  // console.log("set-position", match[2], match[3], match[4])
    riderLogData.updatePosition(match[2], match[4])

  } else if (match = patterns.network.exec(data)) {
	  // network
    if (windows['network']) {
      windows['network'].webContents.send('network', match[1], match[2], match[3]);
    }

  } else if (match = patterns.chat.exec(data)) {
    // chat
    // console.log("chat", match[1], match[2], match[3])
    if (chatData) {
      chatData.newMessage(match[1], match[2], match[3])
    } else {
      // fall back to just sending message directly to chat window
      if (windows['chat']) {
        windows['chat'].webContents.send('chat', match[1], match[2], match[3], '', '');
      }
    }

  } else if (match = patterns.world.exec(data)) {
	  // world
    if (windows['map']) {
      windows['map'].webContents.send('set-world', match[2]);
      locationData.setWorld(match[2])
    }
  } 

});


//
tail.on('error', function(data) {
  console.log("error:", data);
});

tail.watch();
// to unwatch and close all file descriptors, tail.unwatch();
